//
//  ImageUploader.swift
//  Messn
//
//  Created by Marco Margarucci on 07/09/21.
//

import Firebase
import UIKit

struct ImageUploader {
    
    // Upload image
    static func uploadImage(image: UIImage, completion: @escaping(String) -> Void) {
        // Compress image
        guard let imageData = image.jpegData(compressionQuality: 0.5) else { return }
        // Image filename
        let filename = NSUUID().uuidString
        // Firebase Storage reference
        let reference = Storage.storage().reference(withPath: "/profile_images/\(filename)")
        
        reference.putData(imageData, metadata: nil) { _, error in
            if let error = error {
                debugPrint("DEBUG: Failed to upload image with error: \(error.localizedDescription)")
                return
            }
            
            reference.downloadURL { url, _ in
                guard let imageURL = url?.absoluteString else { return }
                completion(imageURL)
            }
        }
    }
}
