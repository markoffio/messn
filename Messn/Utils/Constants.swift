//
//  Constants.swift
//  Messn
//
//  Created by Marco Margarucci on 08/09/21.
//

import Foundation
import Firebase

// Firebase
// Users collection
let COLLECTION_USERS = Firestore.firestore().collection("users")
// Messages collection
let COLLECTION_MESSAGES = Firestore.firestore().collection("messages")
