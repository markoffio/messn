//
//  SearchBar.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI

struct SearchBar: View {
    // MARK: - Properties
    // Search text
    @Binding var text: String
    // Is editing
    @Binding var isEditing: Bool

    var body: some View {
        HStack {
            // Show back button if the search bar is editing
            if isEditing {
                Button(action: {
                    isEditing = false
                    text = ""
                    // Dismiss keyboard
                    UIApplication.shared.endEditing()
                }, label: {
                    Image(systemName: "xmark")
                        .foregroundColor(Color.strongPurple)
                        .frame(width: 22, height: 22)
                })
                .padding(.trailing, 8)
                .transition(.move(edge: .trailing))
                .animation(.easeInOut(duration: 1.0))
            }
            TextField("search...", text: $text)
                .font(.system(size: 15, weight: .semibold))
                .foregroundColor(Color(.darkGray))
                .frame(height: 30)
                .padding(8)
                .padding(.horizontal, 28)
                .background(Color(.white))
                .cornerRadius(25)
                .shadow(color: .gray.opacity(0.5), radius: 5, x: 0.0, y: 1.0)
                .overlay(
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(Color(.darkGray))
                        .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
                        .padding(.leading, 8)
                )
        }
    }
}

struct SearchBar_Previews: PreviewProvider {
    static var previews: some View {
        SearchBar(text: .constant("search..."), isEditing: .constant(false))
    }
}
