//
//  Extension.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import UIKit

extension UIApplication {
    // End editing
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
