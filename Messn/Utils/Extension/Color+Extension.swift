//
//  Color+Extension.swift
//  Tog
//
//  Created by Marco Margarucci on 21/08/21.
//

import Foundation
import SwiftUI

extension Color {
    // Strong purple
    static let strongPurple = Color("strongPurple")
    // Light gray
    static let lightGray = Color("lightGray")
}
