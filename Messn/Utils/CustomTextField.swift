//
//  CustomTextField.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct CustomTextField: View {
    // Email address
    @Binding var text: String
    // Image name
    let imageName: String
    // Placeholder
    let placeholder: String
    // Is secure field
    let isSecureField: Bool
    
    var body: some View {
        VStack(spacing: 16) {
            HStack {
                if isSecureField {
                    Image(systemName: imageName)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .foregroundColor(.gray)
                        .padding(.trailing, 3)
                    SecureField(placeholder, text: $text)
                } else {
                    Image(systemName: imageName)
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .foregroundColor(.gray)
                        .padding(.trailing, 2)
                    TextField(placeholder, text: $text)
                        .keyboardType(.emailAddress)
                }
            }
            Divider()
                .background(Color(.darkGray))
        }
    }
}
