//
//  MessnApp.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI
import Firebase

@main
struct MessnApp: App {
    
    init() {
        // Configure Firebase
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(AuthenticationViewModel.shared)
        }
    }
}
