//
//  ContentView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI
import Firebase
import FirebaseAuth

struct ContentView: View {
    // MARK: - Properties
    
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    var body: some View {
        Group {
            // If the user is already logged in show the main view
            if viewModel.userSession != nil {
                MainTabView()
            } else { // Oterwise show the login view
                LoginView()
            }
        }
        .onAppear {
            debugPrint("***[DEBUG]*** USER TOKEN: \(viewModel.userToken)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
