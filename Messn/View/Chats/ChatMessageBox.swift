//
//  ChatMessageBox.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI

struct ChatMessageBox: Shape {
    // MARK: - Properties
    
    // This is used to check if the message is from the current user
    var isFromCurrentUser: Bool = false
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .topRight, isFromCurrentUser ? .bottomLeft : .bottomRight], cornerRadii: CGSize(width: 16, height: 16))
        return Path(path.cgPath)
    }
}

struct ChatMessageBox_Previews: PreviewProvider {
    static var previews: some View {
        ChatMessageBox(isFromCurrentUser: true)
    }
}
