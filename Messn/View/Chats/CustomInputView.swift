//
//  CustomInputView.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI

struct CustomInputView: View {
    // MARK: - Properties
    
    // Text message
    @Binding var text: String
    // Button action
    var action: () -> Void
    
    var body: some View {
        VStack {
            ZStack {
                TextField("typer your message", text: $text)
                    .font(.system(size: 13, weight: .semibold))
                    .foregroundColor(Color(.gray))
                    .frame(height: 35)
                    .padding(8)
                    .padding(.horizontal, 18)
                    .background(Color(.white))
                    .cornerRadius(25)
                    .shadow(color: .gray.opacity(0.5), radius: 5, x: 0.0, y: 1.0)
                HStack {
                    Spacer()
                    Button(action: action , label: {
                        Image(systemName: "paperplane")
                            .font(.headline)
                            .frame(width: 38, height: 38)
                            .foregroundColor(.white)
                            .cornerRadius(10)
                            .foregroundColor(.white)
                            .background(Color.strongPurple)
                            .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                            .rotationEffect(.degrees(45))
                    })
                    .opacity(text.count > 0 ? 1.0 : 0.0) // Check if the user entered some text and hide the button if not
                }
                .padding(10)
            }
        }
        .padding()
    }
}

/*
struct CustomInputView_Previews: PreviewProvider {
    static var previews: some View {
        CustomInputView(text: .constant("type your message"))
    }
}
*/
