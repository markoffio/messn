//
//  MessageView.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI
import Kingfisher

struct MessageView: View {
    // MARK: - Properties
    
    // Message view model
    let viewModel: MessageViewModel
    
    var body: some View {
        HStack {
            if viewModel.isFromCurrentUser { // The message is from the current user
                Spacer()
                Text(viewModel.message.text)
                    .padding(12)
                    .background(Color.strongPurple.opacity(0.8))
                    .font(.system(size: 13, weight: .semibold))
                    .clipShape(ChatMessageBox(isFromCurrentUser: true))
                    .shadow(color: .gray.opacity(0.7), radius: 5, x: 5.0, y: 5.0)
                    .foregroundColor(Color(.white))
                    .padding(.leading, 100)
                    .padding(.trailing, 10)
            } else { // The message is from the other user
                HStack(alignment: .bottom) {
                    KFImage(viewModel.profileImageURL)
                        .resizable()
                        .scaledToFill()
                        .frame(width: 32, height: 32)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                    Text(viewModel.message.text)
                        .padding(12)
                        .background(Color.white)
                        .font(.system(size: 13, weight: .semibold))
                        .clipShape(ChatMessageBox(isFromCurrentUser: false))
                        .shadow(color: .gray.opacity(0.2), radius: 5, x: -5.0, y: 5.0)
                        .foregroundColor(Color(.darkGray))
                }
                .padding(.horizontal)
                .padding(.trailing, 80)
                Spacer()
            }
        }
    }
}

/*
struct MessageView_Previews: PreviewProvider {
    static var previews: some View {
        MessageView(isFromCurrentUser: false)
    }
}
*/
