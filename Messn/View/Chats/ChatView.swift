//
//  ChatView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct ChatView: View {
    // MARK: - Properties
    
    // Message text
    @State private var messageText: String = ""
    // View model
    @ObservedObject var viewModel: ChatViewModel
    // User
    private let user: User
    
    init(user: User) {
        self.user = user
        self.viewModel = ChatViewModel(user: user)
    }
    
    var body: some View {
        VStack {
            // Messages scroll view
            ScrollView(showsIndicators: false) {
                ScrollViewReader { value in
                    VStack(alignment: .leading, spacing: 12) {
                        ForEach(viewModel.messages, id: \.id) { message in
                            MessageView(viewModel: MessageViewModel(message))
                        }
                        .onChange(of: viewModel.messages.count) { _ in
                            value.scrollTo(viewModel.messages.last?.id)
                        }
                        .onAppear {
                            value.scrollTo(viewModel.messages.last?.id)
                        }
                    }
                }
            }

            // Input view
            CustomInputView(text: $messageText, action: sendMessage)
                .padding(.top, 25)
        }
        .navigationTitle(user.username)
        .navigationBarTitleDisplayMode(.inline)
        .padding(.vertical)
    }
    
    // Send message
    func sendMessage() {
        viewModel.sendMessage(messageText)
        messageText = ""
    }
}

/*
struct ChatView_Previews: PreviewProvider {
    static var previews: some View {
        ChatView(text: .constant("typer you message"))
    }
}
*/
