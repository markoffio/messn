//
//  ChannelsView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct ChannelsView: View {
    var body: some View {
        Text("QUESTA SEZIONE E\' DA COMPLETARE")
            .foregroundColor(.strongPurple)
            .bold()
    }
}

struct ChannelsView_Previews: PreviewProvider {
    static var previews: some View {
        ChannelsView()
    }
}
