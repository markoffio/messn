//
//  SettingsCell.swift
//  Messn
//
//  Created by Marco Margarucci on 30/08/21.
//

import SwiftUI

struct SettingsCell: View {
    // MARK: - Properties
    // Settings cell view model
    let viewModel: SettingsCellViewModel
    
    var body: some View {
        VStack {
            HStack {
                Image(systemName: viewModel.imageName)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 22, height: 22)
                    .padding(6)
                    .background(viewModel.backgroundColor.opacity(0.2))
                    .foregroundColor(.white)
                    .cornerRadius(6)
                Text(viewModel.title)
                    .font(.system(size: 15))
                    .foregroundColor(Color(.darkGray))
                    .bold()
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(.gray)
            }
            .padding([.top, .horizontal])
            Divider()
                .padding(.leading)
        }
    }
}

/*
struct SettingsCell_Previews: PreviewProvider {
    static var previews: some View {
        SettingsCell()
    }
}
 */
