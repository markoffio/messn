//
//  AvatarView.swift
//  Tog
//
//  Created by Marco Margarucci on 24/08/21.
//

import SwiftUI

struct AvatarView: View {
    // Size
    var size: CGFloat
    // Image
    var imageName: Image?
    
    var body: some View {
        Image("joker")
            .resizable()
            .scaledToFill()
            .frame(width: size, height: size)
            .clipShape(Circle())
    }
}

/*
struct AvatarView_Previews: PreviewProvider {
    static var previews: some View {
        AvatarView()
    }
}
*/
