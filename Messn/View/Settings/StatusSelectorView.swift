//
//  StatusSelectorView.swift
//  Messn
//
//  Created by Marco Margarucci on 30/08/21.
//

import SwiftUI

struct StatusSelectorView: View {
    // MARK: - Properties
    
    // Status view model
    @ObservedObject var viewModel = StatusViewModel()
    
    var body: some View {
        // Set status
        ScrollView {
            VStack(alignment: .leading, spacing: 20) {
                Text("CURRENTLY SET TO")
                    .font(.system(size: 15, weight: .bold))
                    .foregroundColor(Color(.darkGray))
                StatusCell(status: viewModel.status)
                Text("SELECT YOU STATUS")
                    .font(.system(size: 15, weight: .bold))
                    .foregroundColor(Color(.darkGray))
                ForEach(UserStatus.allCases.filter({ $0 != .notConfigured }), id: \.self) { status in
                    Button(action: { viewModel.updateStatus(status) }, label: {
                        StatusCell(status: status)
                    })
                }
                Spacer()
            }
            .padding()
        }
        //.padding(.top, 40)
    }
}

struct StatusSelectorView_Previews: PreviewProvider {
    static var previews: some View {
        StatusSelectorView()
    }
}

struct StatusCell: View {
    // User status
    let status: UserStatus
    
    var body: some View {
        HStack {
            Text(status.title)
                .font(.system(size: 15, weight: .semibold))
                .foregroundColor(Color(.gray))
            Spacer()
//            Image(systemName: "circle.fill")
//                .resizable()
//                .scaledToFit()
//                .frame(width: 15, height: 15)
//                .foregroundColor(.green)
        }
        .frame(height: 35)
        .padding(.horizontal)
    }
}
