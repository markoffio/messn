//
//  SettingsView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct SettingsView: View {
    // MARK: - Properties
    
    // Current user
    private let user: User
    
    init(user: User) {
        self.user = user
    }
    
    var body: some View {
        VStack(spacing: 35) {
            NavigationLink(
                destination: EditProfileView(),
                label: { SettingsHeaderView(user: user) })
            VStack(spacing: 1) {
                ForEach(SettingsCellViewModel.allCases, id: \.self) { viewModel in
                    SettingsCell(viewModel: viewModel)
                }
            }
            // Logout button
            Button(action: { AuthenticationViewModel.shared.signout() }, label: {
                Text("LOGOUT")
                    .font(.headline)
                    .fontWeight(.heavy)
                    .foregroundColor(.white)
                    .frame(width: 340, height: 55)
                    .background(Color.strongPurple)
                    .clipShape(Capsule())
                    .padding()
            })
            .shadow(color: .gray, radius: 2, x: 0.0, y: 2.0)
            Spacer()
        }
    }
}

/*
struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
*/
