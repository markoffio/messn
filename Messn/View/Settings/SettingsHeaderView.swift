//
//  SettingsHeaderView.swift
//  Messn
//
//  Created by Marco Margarucci on 30/08/21.
//

import SwiftUI
import Kingfisher

struct SettingsHeaderView: View {
    // MARK: - Properties
    
    // Current user
    private let user: User
    
    init(user: User) {
        self.user = user
    }
    
    var body: some View {
        HStack {
            ZStack {
                KFImage(URL(string: user.profileImageURL))
                    .resizable()
                    .scaledToFill()
                    .frame(width: 64, height: 64)
                    .clipShape(Circle())
                    .padding(.leading)
                Image(systemName: "circle.fill")
                    .foregroundColor(.green)
                    .offset(x: 28, y: 25)
            }
            .padding(.top, 16)
            VStack(alignment: .leading, spacing: 5) {
                Text(user.fullname)
                    .foregroundColor(Color(.darkGray))
                    .font(.system(size: 18))
                    .bold()
                Text("Available")
                    .foregroundColor(.gray)
                    .font(.system(size: 14))
                    .fontWeight(.semibold)
            }
            .padding(.leading, 10)
            Spacer()
        }
    }
}

/*
struct SettingsHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsHeaderView()
    }
}
*/
