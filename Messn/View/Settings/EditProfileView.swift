//
//  EditProfileView.swift
//  Messn
//
//  Created by Marco Margarucci on 30/08/21.
//

import SwiftUI
import Combine

struct EditProfileView: View {
    // MARK: - Properties
    // Full name
    @State private var fullName: String = ""

    // Full name text limit
    let textLimit: Int = 16
    
    var body: some View {
        VStack {
            HeaderEditProfileView()
                .padding()
            HStack {
                TextField("enter your username", text: $fullName)
                    .onReceive(Just(fullName)) { _ in limitText(textLimit) }
                    .font(.system(size: 15, weight: .bold))
                    .foregroundColor(Color(.darkGray))
                    .padding(.leading, 10)
                    .padding([.top, .bottom, .trailing], 16)
                CharactersRemainView(currentCount: fullName.count)
                    .padding(.trailing, 16)
            }
            .padding(.leading, 20)
            Divider()
                .padding(.leading)
            // Set status
            VStack(alignment: .leading, spacing: 20) {
                Text("Status")
                    .font(.system(size: 15, weight: .bold))
                    .foregroundColor(Color(.darkGray))
                NavigationLink(
                    destination: StatusSelectorView(),
                    label: {
                        HStack {
                            Text("Available")
                                .font(.system(size: 15, weight: .semibold))
                                .foregroundColor(Color(.gray))
                            Spacer()
                            Image(systemName: "chevron.right")
                                .foregroundColor(.gray)
                        }
                    })
            }
            .padding()
            Spacer()
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Edit profile")
    }
    
    // Function to keep text length in limits
    func limitText(_ upper: Int) {
        if fullName.count > upper {
            fullName = String(fullName.prefix(upper))
        }
    }
}

struct EditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditProfileView()
    }
}

// DA INSERIE IN UN NUOVO FILE
struct EditImage: View {
    var body: some View {
        Image(systemName: "plus.circle.fill")
            .renderingMode(.original)
            .foregroundColor(.white)
            .cornerRadius(10)
            .overlay(RoundedRectangle(cornerRadius: 10).stroke(Color(.white), lineWidth: 2))
            .offset(x: 28, y: 36)
    }
}

// DA INSERIE IN UN NUOVO FILE
struct CharactersRemainView: View {
    // Characters count
    var currentCount: Int
    
    var body: some View {
            Text("\(16 - currentCount)")
                .font(.system(size: 15, weight: .semibold))
                .foregroundColor(currentCount < 16 ? .green : .strongPurple)
    }
}
