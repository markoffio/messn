//
//  HeaderEditProfileView.swift
//  Messn
//
//  Created by Marco Margarucci on 30/08/21.
//

import SwiftUI

struct HeaderEditProfileView: View {
    // MARK: - Properties
    
    // Show image picker
    @State private var showImagePicker: Bool = false
    // Selected image
    @State private var selectedImage: UIImage?
    // Profile image
    @State private var profileImage: Image?
    
    var body: some View {
        HStack(spacing: 12) {
            ZStack {
                if let profileImage = profileImage {
                    profileImage
                        .resizable()
                        .scaledToFill()
                        .frame(width: 84, height: 84)
                        .clipShape(Circle())
                    EditImage()
                } else {
                    Image(systemName: "person.circle")
                        .resizable()
                        .scaledToFill()
                        .frame(width: 84, height: 84)
                        .opacity(0.2)
                        .clipShape(Circle())
                    EditImage()
                }
            }
            Button(action: { showImagePicker.toggle() }, label: {
                Text("Tap here to change your profile picture")
                    .foregroundColor(Color(.darkGray))
                    .font(.system(size: 15))
                    .fontWeight(.semibold)
                    .padding(.trailing)
                    .lineSpacing(5.0)
            })
            .sheet(isPresented: $showImagePicker, onDismiss: loadImage, content: {
                ImagePicker(image: $selectedImage)
            })
        }
    }
    
    // Load image
    func loadImage() {
        guard let selectedImage = selectedImage else { return }
        profileImage = Image(uiImage: selectedImage)
    }
}

struct HeaderEditProfileView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderEditProfileView()
    }
}
