//
//  MainTabView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct MainTabView: View {
    // MARK: - Properties
    
    // Selected tab index
    @State private var selectedIndex: Int = 0
    // Tab title
    var tabTitle: String {
        switch selectedIndex {
        case 0:
            return "Chats"
        case 1:
            return "Channels"
        case 2:
            return "Settings"
        default:
            return ""
        }
    }
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    init() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.darkGray]
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.darkGray]
        UITabBar.appearance().barTintColor = .white
    }
    
    var body: some View {
        if let user = viewModel.currentUser {
            NavigationView {
                TabView(selection: $selectedIndex) {
                    // Conversation view
                    ConversationsView()
                        .onTapGesture {
                            selectedIndex = 0
                        }
                        .tabItem {
                            Image(systemName: "quote.bubble")
                            Text("Chats")
                        }
                        .tag(0)
                    // Channels view
                    ChannelsView()
                        .onTapGesture {
                            selectedIndex = 1
                        }
                        .tabItem {
                            Image(systemName: "megaphone")
                            Text("Channels")
                                .bold()
                        }
                        .tag(1)
                    // Settings view
                    SettingsView(user: user)
                        .onTapGesture {
                            selectedIndex = 2
                        }
                        .tabItem {
                            Image(systemName: "gear")
                            Text("Settings")
                                .bold()
                        }
                        .tag(2)
                }
                .accentColor(.strongPurple)
                .navigationTitle(tabTitle)
                /* IMPORTANTISSIMO
                .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
                    print("Moving to the background!")}
                 
                 ----------------
                 .onReceive(NotificationCenter.default.publisher(for: UIApplication.willEnterForegroundNotification)) { _ in
                         print("Moving back to the foreground!")
                     }
                */
            }
            .accentColor(.strongPurple)
        }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
