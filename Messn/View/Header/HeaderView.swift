//
//  HeaderView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct HeaderView: View {
    // Title
    let title: String
    // Subtitle
    let subtitle: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack { Spacer() }
            Text(title)
                .font(.largeTitle)
                .fontWeight(.heavy)
                .foregroundColor(Color(.darkGray))
            Text(subtitle)
                .font(.title)
                .bold()
                .foregroundColor(.strongPurple)
        }
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(title: "Title", subtitle: "Subtitle")
    }
}
