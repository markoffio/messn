//
//  RegistrationView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct RegistrationView: View {
    // MARK: - Properties
    // Email
    @State private var email: String = ""
    // Username
    @State private var username: String = ""
    // Full name
    @State private var fullName: String = ""
    // Password
    @State private var password: String = ""
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // Authentication view model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    var body: some View {
        VStack {
            
            NavigationLink(
                destination: ProfilePhotoSelectorView(),
                isActive: $viewModel.didAuthenticateUser,
                label: { })
            
            VStack(alignment: .leading, spacing: 10) {
                // Header view
                HeaderView(title: "Are you ready?", subtitle: "create an account")
                VStack(spacing: 30) {
                    // Email text field
                    CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                        .autocapitalization(.none)
                    // Username text field
                    CustomTextField(text: $username, imageName: "person.circle", placeholder: "username", isSecureField: false)
                        .keyboardType(.default)
                        .autocapitalization(.none)
                    // Full name text field
                    CustomTextField(text: $fullName, imageName: "face.smiling", placeholder: "full name", isSecureField: false)
                        .keyboardType(.default)
                    // Password secure field
                    CustomTextField(text: $password, imageName: "key", placeholder: "password", isSecureField: true)

                }
                .padding(.top, 35)
                .padding(.leading, 2)
                .padding(.trailing, 30)
            }
            .padding(.leading, 30)
            // Sign up button
            Button(action: { viewModel.register(withEmail: email, password: password, fullname: fullName, username: username) }, label: {
                Text("SIGN UP")
                    .font(.headline)
                    .fontWeight(.heavy)
                    .foregroundColor(.white)
                    .frame(width: 340, height: 55)
                    .background(Color.strongPurple)
                    .clipShape(Capsule())
                    .padding()
            })
            .shadow(color: .gray, radius: 2, x: 0.0, y: 2.0)
            .padding(.top, 35)
            Spacer()
            Button(action: {
                presentationMode.wrappedValue.dismiss()
            }, label: {
                HStack {
                    Text("Already have an account?")
                        .font(.subheadline)
                        .fontWeight(.semibold)
                        .foregroundColor(.gray)
                    Text("Sign in.")
                        .font(.subheadline)
                        .fontWeight(.bold)
                        .foregroundColor(.strongPurple)
                }
            })
                .padding(.bottom, 35)
        }
    }
}

struct RegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}
