//
//  ProfilePhotoSelectorView.swift
//  Messn
//
//  Created by Marco Margarucci on 06/09/21.
//

import SwiftUI

struct ProfilePhotoSelectorView: View {
    // MARK: - Properties
    
    // Image picker presented
    @State private var imagePickerPresented: Bool = false
    // Selected image
    @State private var selectedImage: UIImage?
    // Profile image
    @State private var profileImage: Image?
    // Authentication view model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    var body: some View {
        VStack {
            Button(action: { imagePickerPresented.toggle() }, label: {
                if let profileImage = profileImage {
                    profileImage
                        .resizable()
                        .scaledToFill()
                        .frame(width: 180, height: 180)
                        .clipShape(Circle())
                        .padding(.top, 44)
                } else {
                    Image(systemName: "person.circle")
                        .resizable()
                        .scaledToFill()
                        .foregroundColor(Color(.darkGray).opacity(0.5))
                        .font(.system(size: 13, weight: .ultraLight))
                        .frame(width: 180, height: 180)
                        .clipped()
                        .padding(.top, 44)
                }
            })
            .sheet(isPresented: $imagePickerPresented, onDismiss: loadImage, content: {
                ImagePicker(image: $selectedImage)
            })
            Text(profileImage == nil ? "Select a profile photo" : "Tap below to continue")
                .font(.system(size: 15, weight: .bold))
                .foregroundColor(Color(.darkGray))
                .padding(.top, 10)
            
            if let image = selectedImage {
                // Continue button
                Button(action: { viewModel.uploadProfileImage(image) }, label: {
                    Text("CONTINUE")
                        .font(.headline)
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        .frame(width: 340, height: 55)
                        .background(Color.strongPurple)
                        .clipShape(Capsule())
                        .padding()
                })
                .shadow(color: .gray, radius: 2, x: 0.0, y: 2.0)
                .padding(.top, 10)
            }
            Spacer()
        }
        .navigationBarBackButtonHidden(true)
    }
    
    // Load image
    func loadImage() {
        guard let selectedImage = selectedImage else { return }
        profileImage = Image(uiImage: selectedImage)
    }
}

struct ProfilePhotoSelectorView_Previews: PreviewProvider {
    static var previews: some View {
        ProfilePhotoSelectorView()
    }
}
