//
//  LoginView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct LoginView: View {
    // MARK: - Properties
    // Email address
    @State private var email: String = ""
    // Password
    @State private var password: String = ""
    // Authentication view model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    var body: some View {
        NavigationView {
            VStack {
                VStack(alignment: .leading, spacing: 10) {
                    // Header view
                    HeaderView(title: "Hi there ;-)", subtitle: "let\'s start a conversation")
                    VStack(spacing: 20) {
                        // Email text field
                        CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                            .keyboardType(.default)
                            .autocapitalization(.none)
                        // Password secure field
                        CustomTextField(text: $password, imageName: "key", placeholder: "password", isSecureField: true)
                    }
                    .padding(.top, 35)
                    .padding(.leading, 2)
                    .padding(.trailing, 30)
                }
                .padding(.leading, 30)
                HStack {
                    Spacer()
                    NavigationLink(
                        destination: Text("Reset password"),
                        label: {
                            Text("Forgot password?")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                        })
                }
                .padding(.top)
                .padding(.trailing, 30)
                .padding(.bottom, 10)
                // Login button
                Button(action: { viewModel.login(withEmail: email, password: password) }, label: {
                    Text("SIGN IN")
                        .font(.headline)
                        .fontWeight(.heavy)
                        .foregroundColor(.white)
                        .frame(width: 340, height: 55)
                        .background(Color.strongPurple)
                        .clipShape(Capsule())
                        .padding()
                })
                .shadow(color: .gray, radius: 2, x: 0.0, y: 2.0)
                Spacer()
                NavigationLink(
                    destination: RegistrationView().navigationBarBackButtonHidden(true),
                    label: {
                        HStack {
                            Text("Don\'t have an account?")
                                .font(.subheadline)
                                .fontWeight(.semibold)
                                .foregroundColor(.gray)
                            Text("Sign up.")
                                .font(.subheadline)
                                .fontWeight(.bold)
                                .foregroundColor(.strongPurple)
                        }
                    })
                    .padding(.bottom, 35)
            }
        }
        .padding(.top, -60)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
