//
//  ConversationCell.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI
import Kingfisher

struct ConversationCell: View {
    // MARK: - Properties
    
    // Conversation cell view model
    @ObservedObject var viewModel: ConversationCellViewModel
    
    var body: some View {
        if let user = viewModel.message.user {
            NavigationLink(destination: ChatView(user: user)) {
                VStack {
                    HStack {
                        // Image
                        KFImage(viewModel.receivingUserProfileImage)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 48, height: 48)
                            .clipShape(Circle())
                        // Message info
                        VStack(alignment: .leading, spacing: 5) {
                            Text(viewModel.fullname)
                                .font(.system(size: 15, weight: .bold))
                                .foregroundColor(Color(.darkGray))
                            Text(viewModel.message.text)
                                .font(.system(size: 13, weight: .semibold))
                                .foregroundColor(Color(.gray))
                        }
                        Spacer()
                    }
                    Divider()
                }
                .padding(.leading)
                .padding(.top)
            }
        }
    }
}

/*
 struct ConversationCell_Previews: PreviewProvider {
 static var previews: some View {
 ConversationCell()
 }
 }
 */
