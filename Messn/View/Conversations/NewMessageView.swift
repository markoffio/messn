//
//  NewMessageView.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI

struct NewMessageView: View {
    // MARK: - Properties
    
    // Show chat view
    @Binding var showChatView: Bool
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // Search text
    @State private var searchText: String = ""
    // Search bar is editing
    @State private var isEditing: Bool = false
    // New message view model
    @ObservedObject var viewModel = NewMessageViewModel()
    // User
    @Binding var user: User?
    
    var body: some View {
        // Messages list
        VStack {
            SearchBar(text: $searchText, isEditing: $isEditing)
                .onTapGesture { isEditing.toggle() }
                .padding()
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading) {
                    HStack { Spacer() }
                    ForEach(viewModel.users) { user in
                        Button(action: {
                            showChatView.toggle()
                            self.user = user
                            presentationMode.wrappedValue.dismiss()
                        }, label: {
                            UserCell(user: user)
                        })
                    }
                }
            }
        }
    }
}

/*
struct NewMessageView_Previews: PreviewProvider {
    static var previews: some View {
        NewMessageView(showChatView: .constant(true))
    }
}
*/
