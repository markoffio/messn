//
//  ConversationsView.swift
//  Messn
//
//  Created by Marco Margarucci on 28/08/21.
//

import SwiftUI

struct ConversationsView: View {
    // MARK: - Properties
    
    // Show new message view
    @State private var showNewMessageView: Bool = false
    // Show chat view
    @State private var showChatView: Bool = false
    // Selected user
    @State var selectedUser: User?
    // Conversation view model
    @ObservedObject var viewModel = ConversationsViewModel()
    
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            if let user = selectedUser {
                NavigationLink(
                    destination: ChatView(user: user),
                    isActive: $showChatView,
                    label: {})
            }
            // Chat list
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading) {
                    HStack { Spacer() }
                    ForEach(viewModel.recentMessages) { message in
                        ConversationCell(viewModel: ConversationCellViewModel(message))
                    }
                }
            }
            // Floating button
            Button(action: {
                showNewMessageView.toggle()
            }, label: {
                Image(systemName: "quote.bubble")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 24, height: 24)
                    .padding()
            })
            .background(Color.strongPurple)
            .foregroundColor(.white)
            .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            .shadow(color: .gray, radius: 5, x: 2.0, y: 5.0)
            .padding()
            .sheet(isPresented: $showNewMessageView, content: {
                NewMessageView(showChatView: $showChatView, user: $selectedUser)
            })
        }
        .onAppear {
            viewModel.fetchRecentMessages()
        }
    }
}

/*
struct ConversationsView_Previews: PreviewProvider {
    static var previews: some View {
        ConversationsView()
    }
}
*/
