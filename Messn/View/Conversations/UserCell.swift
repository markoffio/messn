//
//  UserCell.swift
//  Messn
//
//  Created by Marco Margarucci on 03/09/21.
//

import SwiftUI
import Kingfisher

struct UserCell: View {
    // MARK: - Properties
    
    // Users
    let user: User
    
    var body: some View {
        VStack {
            HStack {
                // Image
                KFImage(URL(string: user.profileImageURL))
                    .resizable()
                    .scaledToFill()
                    .frame(width: 48, height: 48)
                    .clipShape(Circle())
                // Message info
                VStack(alignment: .leading, spacing: 5) {
                    Text(user.username)
                        .font(.system(size: 15, weight: .bold))
                        .foregroundColor(Color(.darkGray))
                    Text(user.fullname)
                        .font(.system(size: 13, weight: .semibold))
                        .foregroundColor(Color(.gray))
                }
                Spacer()
            }
        }
        .padding(.leading)
        .padding(.top)
    }
}

/*
struct UserCell_Previews: PreviewProvider {
    static var previews: some View {
        UserCell()
    }
}
*/
