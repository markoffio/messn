//
//  ConversationsViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

class ConversationsViewModel: ObservableObject {
    // MARK: - Properties
    
    // Recent messages
    @Published var recentMessages = [Message]()
    
    init() {
        fetchRecentMessages()
    }
    
    // Fetch recent messaged
    func fetchRecentMessages() {
        // Get current user id
        guard let uid = AuthenticationViewModel.shared.userSession?.uid else { return }
        
        // Fetch the most recent messages from "recent-messages" collection
        let query = COLLECTION_MESSAGES.document(uid)
                        .collection("recent-messages")
                        .order(by: "timestamp", descending: true)
        query.getDocuments { querySnapshot, _ in
            guard let documents = querySnapshot?.documents else { return }
            self.recentMessages = documents.compactMap({ try? $0.data(as: Message.self) })
        }
    }
}
