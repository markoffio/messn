//
//  ChatViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 04/09/21.
//

import Firebase

class ChatViewModel: ObservableObject {
    // MARK : - Properties
    
    // Messages
    @Published var messages = [Message]()
    // Receiving sser
    let user: User
    
    init(user: User) {
        self.user = user
        fetchMessages()
    }
    
    // Send message
    func sendMessage(_ messageText: String) {
        // Get current user id
        guard let currentUid = AuthenticationViewModel.shared.userSession?.uid else { return }
        // Get receiving user id
        guard let receivingUid = user.id else { return }
        // Current user reference
        let currentUserRef = COLLECTION_MESSAGES.document(currentUid).collection(receivingUid).document()
        // Receiving user reference
        let receivingUserRef = COLLECTION_MESSAGES.document(receivingUid).collection(currentUid)
        // Recent messages collection of current user
        let recentCurrentUserReference = COLLECTION_MESSAGES.document(currentUid).collection("recent-messages").document(receivingUid)
        // Recent messages collection from receiving user
        let recentReceivingUserReference = COLLECTION_MESSAGES.document(receivingUid).collection("recent-messages").document(currentUid)
        // Message id
        let messageId = currentUserRef.documentID
        // Message data
        let data: [String: Any] = ["text": messageText, "fromId": currentUid, "toId": receivingUid, "read": false, "timestamp": Timestamp(date: Date())]
        currentUserRef.setData(data)
        receivingUserRef.document(messageId).setData(data)
        // Set recent messages
        recentCurrentUserReference.setData(data)
        recentReceivingUserReference.setData(data)
    }
    
    // Fetch messages
    func fetchMessages() {
        // Get current user id
        guard let currentUid = AuthenticationViewModel.shared.userSession?.uid else { return }
        // Get receiving user id
        guard let receivingUid = user.id else { return }
        
        
        let query = COLLECTION_MESSAGES
                        .document(currentUid)
                        .collection(receivingUid)
                        .order(by: "timestamp", descending: false)
        
        // Add snapshot listener to the collection of messages
        query.addSnapshotListener { snapshot, _ in
            guard let changes = snapshot?.documentChanges.filter({ $0.type == .added }) else { return }
            var messages = changes.compactMap{ try? $0.document.data(as: Message.self) }
            
            // The message is not from the currently logged in user
            for (index, message) in messages.enumerated() where message.fromId != currentUid {
                messages[index].user = self.user
            }
            self.messages.append(contentsOf: messages)
        }
    }
}
