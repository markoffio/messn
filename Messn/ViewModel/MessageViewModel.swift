//
//  MessageViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 09/09/21.
//

import Foundation


struct MessageViewModel {
    // MARK: - Properties
    
    // Message
    let message: Message
    
    init(_ message: Message) {
        self.message = message
    }
    
    // MARK: - Computed properties
    
    // Current user id
    var currentUid: String {
        return AuthenticationViewModel.shared.userSession?.uid ?? ""
    }
    
    // Used to check if the message is from the current user
    var isFromCurrentUser: Bool {
        return message.fromId == currentUid
    }
    
    // Profile user image url
    var profileImageURL: URL? {
        guard let profileImageURL = message.user?.profileImageURL else { return nil }
        return URL(string: profileImageURL)
    }
}
