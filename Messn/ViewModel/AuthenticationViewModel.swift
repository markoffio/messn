//
//  AuthenticationViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 06/09/21.
//

import Firebase
import FirebaseAuth
import UIKit

class AuthenticationViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    
    // Used to check if the user is authenticated
    @Published var didAuthenticateUser: Bool = false
    // Temporary current user
    private var tempCurrentUser: FirebaseAuth.User?
    // User session
    @Published var userSession: FirebaseAuth.User?
    // Current user
    @Published var currentUser: User?
    // User token
    @Published var userToken: String = ""
    // Shared instance
    static let shared = AuthenticationViewModel()
    
    // Initializer
    override init() {
        super.init()
        // Initialize user session
        userSession = Auth.auth().currentUser
        fetchUser()
    }
    
    // Login user
    func login(withEmail email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) { authDataResult, error in
            // Check for errors
            if let error = error {
                debugPrint("DEBUG: Failed to register with error: \(error.localizedDescription)")
                return
            }
            guard let user = authDataResult?.user else { return }
            self.userSession = user
            user.getIDTokenForcingRefresh(false, completion: { idToken, error in
                if let error = error {
                    debugPrint("***[DEBUG]*** CANNOT GET USER TOKEN: \(error.localizedDescription)")
                    return
                }
                self.userToken = idToken ?? "NO TOKEN"
            })
            self.fetchUser()
        }
    }
    
    // Register user
    func register(withEmail email: String, password: String, fullname: String, username: String) {
        Auth.auth().createUser(withEmail: email, password: password) { authDataResult, error in
            // Check for errors
            if let error = error {
                debugPrint("DEBUG: Failed to register with error: \(error.localizedDescription)")
                return
            }
            
            guard let user = authDataResult?.user else { return }
            self.tempCurrentUser = user

            let data: [String: Any] = ["email": email, "username": username, "fullname": fullname]
            
            COLLECTION_USERS.document(user.uid).setData(data) { _ in
                self.didAuthenticateUser = true
            }
        }
    }
    
    // Upload profile image
    func uploadProfileImage(_ image: UIImage) {
        guard let uid = tempCurrentUser?.uid else { return }
        ImageUploader.uploadImage(image: image) { imageURL in
            COLLECTION_USERS.document(uid).updateData(["profileImageURL": imageURL]) { _ in
                self.userSession = self.tempCurrentUser
            }
        }
    }
    
    // Sign out
    func signout() {
        self.userSession = nil
        do {
            try Auth.auth().signOut()
        } catch let error {
            debugPrint("DEBUG: Error signing out the user: \(error.localizedDescription)")
        }
    }
    
    // Fetch user
    func fetchUser() {
        // Get current user id
        guard let uid = userSession?.uid else { return }
        COLLECTION_USERS.document(uid).getDocument { snapshot, _ in
            guard let user = try? snapshot?.data(as: User.self) else { return }
            self.currentUser = user
        }
    }
}
