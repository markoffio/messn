//
//  ConversationCellViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 15/09/21.
//

import SwiftUI

class ConversationCellViewModel: ObservableObject {
    // MARK: - Properties
    
    // Message
    @Published var message: Message
    
    init(_ message: Message) {
        self.message = message
        fetchUser()
    }
    
    // Receiving user id: get the message from the user we are chatting to
    var receivingUserId: String {
        // The message is from the current user
        return message.fromId == AuthenticationViewModel.shared.userSession?.uid ? message.toId : message.fromId
    }
    
    // Receiving user profile image
    var receivingUserProfileImage: URL? {
        guard let user = message.user else { return nil }
        return URL(string: user.profileImageURL)
    }
    
    // User fullname
    var fullname: String {
        guard let user = message.user else { return "" }
        return user.fullname
    }
    
    // MARK: - Functions
    
    // Fetch user
    func fetchUser() {
        COLLECTION_USERS.document(receivingUserId).getDocument { snapshot, _ in
            self.message.user = try? snapshot?.data(as: User.self)
        }
    }
}
