//
//  SettingsCellViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 30/08/21.
//

import Foundation
import SwiftUI

enum SettingsCellViewModel: Int, CaseIterable {
    case account
    case notifications
    case starredMessages
    
    // Title
    var title: String {
        switch self {
        case .account:
            return "Account"
        case .notifications:
            return "Notifications"
        case .starredMessages:
            return "Starred messages"
        }
    }
    
    // Image name
    var imageName: String {
        switch self {
        case .account:
            return "person.fill"
        case .notifications:
            return "bell.badge.fill"
        case .starredMessages:
            return "star.fill"
        }
    }

    // Background color
    var backgroundColor: Color {
        switch self {
        case .account:
            return .blue
        case .notifications:
            return .red
        case .starredMessages:
            return .yellow
        }
    }
}
