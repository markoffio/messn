//
//  NewMessageViewModel.swift
//  Messn
//
//  Created by Marco Margarucci on 08/09/21.
//

import SwiftUI
import Firebase

class NewMessageViewModel: ObservableObject {
    // MARK: - Properties
    
    // Users
    @Published var users = [User]()
    
    init() {
        fetchUsers()
    }
    
    // Fetch users
    func fetchUsers() {
        COLLECTION_USERS.getDocuments { querySnapshot, _ in
            guard let documents = querySnapshot?.documents else { return }
            self.users = documents.compactMap({ try? $0.data(as: User.self) })
                .filter({ $0.id != AuthenticationViewModel.shared.userSession?.uid }) // Filter current user, removing it from the array
        }
    }
}
