//
//  Message.swift
//  Messn
//
//  Created by Marco Margarucci on 04/09/21.
//

import Firebase
import FirebaseFirestoreSwift

struct Message: Identifiable, Decodable {
    // MARK: - Properties
    
    // Document ID
    @DocumentID var id: String?
    // From Id
    let fromId: String
    // To id
    let toId: String
    // Read
    let read: Bool
    // Message text
    let text: String
    // User
    var user: User?
}
