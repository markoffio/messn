//
//  User.swift
//  Messn
//
//  Created by Marco Margarucci on 07/09/21.
//


import FirebaseFirestoreSwift

struct User: Identifiable, Decodable {
    // MARK: - Properties
    
    // User id
    @DocumentID var id: String?
    // Username
    let username: String
    // Fullname
    let fullname: String
    // Email
    let email: String
    // Profile image URL
    let profileImageURL: String
}

let MOCK_USER = User(id: "oy7fo87xrf788or", username: "Test", fullname: "test", email: "test@gmail.com", profileImageURL: "")
